using MyCode;
using NUnit.Framework;

namespace Tests
{
    public class Tests
    {
        private Class1 _myClass;

        [SetUp]
        public void Setup()
        {
            _myClass = new Class1();
        }

        [Test]
        public void Test1()
        {
            //Arrange
            //Act
            string result = _myClass.MyOtherMethod(-1);
            
            //Assert
            Assert.AreEqual("1.00", result);
        }
    }
}