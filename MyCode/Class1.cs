﻿using System;
using System.Globalization;

namespace MyCode
{
    public class Class1
    {
        public string MyMethod(int i)
        {
            if (i % 2 == 0)
                return i.ToString(CultureInfo.InvariantCulture);
            else
                return (i * -1).ToString(CultureInfo.InvariantCulture);
        }

        public string MyOtherMethod(decimal d)
        {
            if (decimal.Zero == d)
            {
                return 5d.ToString("F", CultureInfo.CurrentCulture);
            }
            else if (d > Decimal.Zero)
            {
                return d.ToString("F", CultureInfo.InvariantCulture);
            }
            else
            {
                return (d * -1).ToString("F", CultureInfo.InvariantCulture);
            }
        }
    }
}