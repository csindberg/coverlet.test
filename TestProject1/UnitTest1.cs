using System;
using MyCode;
using Xunit;

namespace TestProject1
{
    public class UnitTest1
    {
        private Class1 _myClass;

        public UnitTest1()
        {
            _myClass = new Class1();
        }
        
        [Fact]
        public void TestUnevenIsNegative()
        {
            //Arrange
            //Act
            string result = _myClass.MyMethod(1);
            
            //Assert
            Assert.Equal("-1", result);
        }
    }
}
